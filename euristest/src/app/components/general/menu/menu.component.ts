import { Component, OnInit } from '@angular/core';
import { MenuService } from 'src/app/services/menu.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent implements OnInit {
  isMenuOpen: boolean = false;
  hasBackdrop: boolean = false;
  title: string;

  constructor(private menuService: MenuService) {}

  ngOnInit(): void {
    this.setMenu();
    this.getTitle();
  }

  setMenu() {
    // this.menuService.isOpen().subscribe((res) => {
    //   this.isMenuOpen = res;
    // });
  }

  toggleMenu() {
    // this.menuService.toggleOpen();
  }

  getTitle() {
    this.menuService.getTitle().subscribe((title) => (this.title = title));
  }
}
