import { Component, OnInit, Input } from '@angular/core';
import { StoreLayout } from 'src/app/interfaces/api-response';
import { DashboardService } from 'src/app/pages/dashboard/service/dashboard.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  @Input() storeData: StoreLayout;
  @Input() viewMode: string;

  constructor(private dashboardService: DashboardService) {}

  ngOnInit(): void {
    this.dashboardService
      .getViewMode()
      .subscribe((res) => (this.viewMode = res));
  }

  viewModeChange(value: string) {
    this.viewMode = value;
    this.dashboardService.setViewMode(value);
  }
}
