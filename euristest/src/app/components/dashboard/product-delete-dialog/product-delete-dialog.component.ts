import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Product } from 'src/app/interfaces/api-response';

@Component({
  selector: 'app-product-delete-dialog',
  templateUrl: './product-delete-dialog.component.html',
  styleUrls: ['./product-delete-dialog.component.scss'],
})
export class ProductDeleteDialogComponent implements OnInit {
  testo: string;

  constructor(
    public dialogRef: MatDialogRef<ProductDeleteDialogComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: { productData: Product[] }
  ) {}

  ngOnInit(): void {
    if (this.data.productData.length === 1) {
      this.testo = this.data.productData[0].title;
    } else {
      this.testo = this.data.productData.length + ' elementi';
    }
  }
}
