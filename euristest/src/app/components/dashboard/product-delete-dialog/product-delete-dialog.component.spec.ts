import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductDeleteDialogComponent } from './product-delete-dialog.component';
import {
  MatDialogModule,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';

describe('ProductDeleteDialogComponent', () => {
  let component: ProductDeleteDialogComponent;
  let fixture: ComponentFixture<ProductDeleteDialogComponent>;

  const mockDialogRef = {
    productId: '',
    productData: {},
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProductDeleteDialogComponent],
      imports: [MatDialogModule],
      providers: [
        {
          provide: MatDialogRef,
          useValue: {},
        },
        {
          provide: MAT_DIALOG_DATA,
          useValue: mockDialogRef,
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductDeleteDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
