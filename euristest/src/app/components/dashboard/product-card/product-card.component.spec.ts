import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductCardComponent } from './product-card.component';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { DashboardComponent } from 'src/app/pages/dashboard/dashboard.component';
import { HttpClientModule } from '@angular/common/http';
import { MatDialogModule } from '@angular/material/dialog';
import { MatMenuModule } from '@angular/material/menu';

describe('ProductCardComponent', () => {
  let component: ProductCardComponent;
  let fixture: ComponentFixture<ProductCardComponent>;
  let router: Router;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProductCardComponent],
      providers: [DashboardComponent],
      imports: [
        RouterTestingModule,
        HttpClientModule,
        MatDialogModule,
        MatMenuModule,
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductCardComponent);
    component = fixture.componentInstance;
    router = TestBed.get(Router);

    component.data = {
      title: 'string',
      category: 'string',
      price: 0,
      employee: 'string',
      description: 'string',
    };
    component.id = 'id-123';
    component.viewMode = 'list';
    component.isSelected = false;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  // it("se clicco su una scheda mi apre l'url", () => {
  //   spyOn(router, 'navigateByUrl');
  //   component.onCardClick();
  //   expect(router.navigateByUrl).toHaveBeenCalledWith('/dashboard/id-123');
  // });
});
