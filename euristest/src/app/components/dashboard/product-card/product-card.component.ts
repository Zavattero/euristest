import { Component, OnInit, Input } from '@angular/core';
import { Product } from 'src/app/interfaces/api-response';
import { Routes } from 'src/app/enums/routes.enum';
import { DashboardComponent } from 'src/app/pages/dashboard/dashboard.component';
import { MatDialog } from '@angular/material/dialog';
import { ProductDeleteDialogComponent } from '../product-delete-dialog/product-delete-dialog.component';
import { DashboardService } from 'src/app/pages/dashboard/service/dashboard.service';

@Component({
  selector: 'app-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.scss'],
})
export class ProductCardComponent implements OnInit {
  @Input() data: Product;
  @Input() id: string;
  @Input() viewMode: 'list' | 'grid';
  @Input() isNewProduct: boolean;

  isDeleted = false;

  isSelected = false;

  isMultipleSelectActive = false;
  isMultiSelected = false;

  linkUrl: string;

  constructor(
    private parent: DashboardComponent,
    private dashboardService: DashboardService,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.parent.currentProductId === this.id;
    this.linkUrl = [Routes.DASHBOARD, this.id, Routes.REVIEW].join('/');
    this.checkMultipleSelect();
  }

  onDelete(e) {
    const dialogRef = this.dialog.open(ProductDeleteDialogComponent, {
      data: {
        productData: [this.data],
      },
    });

    dialogRef.afterClosed().subscribe((hasToRemove) => {
      if (hasToRemove) {
        this.dashboardService.deleteProduct(this.id).subscribe((res) => {
          this.isDeleted = true;
          this.dashboardService.callProductList();
        });
      }
    });
  }

  onMultipleSelect(e) {
    this.dashboardService.setMultipleSelect(true);
    this.dashboardService.addToMultipleSelect(this.id);
  }

  onMultiSelectToggle(e) {
    this.dashboardService.toggleMultipleSelect(this.id);
  }

  checkMultipleSelect() {
    this.dashboardService
      .getMultipleSelect()
      .subscribe((res) => (this.isMultipleSelectActive = res));

    this.dashboardService
      .getMultipleSelectArr()
      .subscribe(
        (_res) =>
          (this.isMultiSelected = this.dashboardService.isInMultipleSelect(
            this.id
          ))
      );
  }
}
