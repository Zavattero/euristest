import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { DashboardService } from './service/dashboard.service';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { ProductCardComponent } from '../../components/dashboard/product-card/product-card.component';
import { MatDialogModule } from '@angular/material/dialog';
import { ProductComponent } from './pages/product/product.component';
import { ProductDialogComponent } from './pages/product-dialog/product-dialog.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { MatListModule } from '@angular/material/list';
import { HeaderComponent } from '../../components/dashboard/header/header.component';
import { ProductNewComponent } from './pages/product-new/product-new.component';
import { ProductNewDialogComponent } from './pages/product-new-dialog/product-new-dialog.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatMenuModule } from '@angular/material/menu';
import { ProductDeleteDialogComponent } from '../../components/dashboard/product-delete-dialog/product-delete-dialog.component';

@NgModule({
  declarations: [
    DashboardComponent,
    ProductCardComponent,
    ProductComponent,
    ProductDialogComponent,
    HeaderComponent,
    ProductNewComponent,
    ProductNewDialogComponent,
    ProductDeleteDialogComponent,
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    MatCardModule,
    MatIconModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatTooltipModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatBottomSheetModule,
    MatListModule,
    FormsModule,
    ReactiveFormsModule,
    MatAutocompleteModule,
    MatMenuModule,
  ],
  providers: [DashboardService],
})
export class DashboardModule {}
