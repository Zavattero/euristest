import { Injectable, Component } from '@angular/core';
import { EndpointsService } from 'src/app/services/endpoints.service';
import { ApiService } from 'src/app/services/api.service';
import { StoreService } from '../../store/service/store.service';
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import { Store, Products, ProductSave } from 'src/app/interfaces/api-response';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class DashboardService {
  private currentStoreId: string;
  private currentStore: Subject<Store> = new Subject();
  private viewMode: BehaviorSubject<string> = new BehaviorSubject(
    localStorage.getItem('viewMode') || 'list'
  );
  private _currentProductId: Subject<string> = new Subject();
  private _productList: BehaviorSubject<Products[]> = new BehaviorSubject([]);
  private _newProductId: Subject<string> = new Subject();
  private _multipleSelectActive: BehaviorSubject<boolean> = new BehaviorSubject(
    false
  );
  private _multipleSelectArr: Set<string> = new Set();
  private _multipleSelect: BehaviorSubject<Set<string>> = new BehaviorSubject(
    this._multipleSelectArr
  );

  constructor(
    private endpointService: EndpointsService,
    private apiService: ApiService,
    private storeService: StoreService
  ) {}

  getCurrentStore() {
    this.currentStoreId = this.storeService.getcurrentStore();
    return this.currentStoreId;
  }

  getStoreData(): Observable<Store> {
    const currentStore = this.storeService.getCurrentStoreData();
    if (typeof currentStore === 'undefined') {
      this.storeService.getStoreById(this.currentStoreId).subscribe((res) => {
        this.currentStore.next(res);
      });
    } else {
      this.currentStore.next(currentStore);
    }
    return this.currentStore.asObservable();
  }

  getProductsList(
    update = true,
    storeId = this.currentStoreId
  ): Observable<any> {
    if (update) {
      this.callProductList(storeId);
    }
    return this._productList.asObservable();
  }

  callProductList(storeId = this.currentStoreId) {
    const productsUrl = this.endpointService.product(storeId);
    this.apiService.get(productsUrl).subscribe((res) => {
      this._productList.next(res);
    });
  }

  getProductById(
    productId: string,
    storeId = this.currentStoreId
  ): Observable<any> {
    const productUrl = this.endpointService.product(storeId, productId);
    return this.apiService.get(productUrl);
  }

  getViewMode() {
    return this.viewMode.asObservable();
  }

  setViewMode(mode) {
    localStorage.setItem('viewMode', mode);
    this.viewMode.next(mode);
  }

  getCurrentProductId(): Observable<string> {
    return this._currentProductId.asObservable();
  }

  setCurrentProductId(productId: string) {
    this._currentProductId.next(productId);
  }

  getNewProductId(): Observable<string> {
    return this._newProductId.asObservable();
  }

  setNewProductId(id: string) {
    this._newProductId.next(id);
  }

  filterArray(arr) {
    arr = [...new Set(arr)];
    arr = arr.filter((item, index) => arr.indexOf(item) === index);
    arr = arr.reduce(
      (unique, item) => (unique.includes(item) ? unique : [...unique, item]),
      []
    );

    return arr;
  }

  getCategoryList(productList: Products[]): string[] {
    const arrCategory = productList.map((el: Products) => {
      return el.data.category;
    });
    return this.filterArray(arrCategory);
  }

  getEmployeesList(productList: Products[]): string[] {
    const arrEmployees = productList.map((el: Products) => {
      return el.data.employee;
    });
    return this.filterArray(arrEmployees);
  }

  saveNewProduct(object: ProductSave, storeId = this.currentStoreId) {
    const productUrl = this.endpointService.product(storeId);
    return this.apiService.post(productUrl, object, { responseType: 'text' });
  }

  deleteProduct(productId: string, storeId = this.currentStoreId) {
    const productUrl = this.endpointService.product(storeId, productId);
    return this.apiService.delete(productUrl);
  }

  getMultipleSelect(): Observable<boolean> {
    return this._multipleSelectActive.asObservable();
  }

  setMultipleSelect(active: boolean): void {
    this._multipleSelectActive.next(active);
    if (!active) {
      this._multipleSelectArr.clear();
    }
  }

  isInMultipleSelect(id: string): boolean {
    return this._multipleSelectArr.has(id);
  }
  addToMultipleSelect(id: string) {
    this._multipleSelectArr.add(id);
    this._multipleSelect.next(this._multipleSelectArr);
  }
  removeFromMultipleSelect(id: string) {
    this._multipleSelectArr.delete(id);
    this._multipleSelect.next(this._multipleSelectArr);
  }
  toggleMultipleSelect(id: string) {
    if (this.isInMultipleSelect(id)) {
      this.removeFromMultipleSelect(id);
    } else {
      this.addToMultipleSelect(id);
    }
  }

  toggleAllMultiple(arr: string[]) {
    if (arr.length === this._multipleSelectArr.size) {
      this._multipleSelectArr.clear();
    } else {
      arr.forEach((el) => {
        this._multipleSelectArr.add(el);
      });
    }
    this._multipleSelect.next(this._multipleSelectArr);
  }

  getMultipleSelectArr(): Observable<Set<string>> {
    return this._multipleSelect.asObservable();
  }
}
