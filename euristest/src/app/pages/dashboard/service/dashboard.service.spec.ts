import { TestBed } from '@angular/core/testing';

import { DashboardService } from './dashboard.service';
import { HttpClientModule } from '@angular/common/http';
import { ApiService } from 'src/app/services/api.service';
import { of } from 'rxjs';

describe('DashboardService', () => {
  let service: DashboardService;
  let apiService: ApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
    });
    service = TestBed.inject(DashboardService);
    apiService = TestBed.inject(ApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('quando è chiamata la product list mi aspetto un array ["stores", "id-store","products"]', () => {
    spyOn(apiService, 'get').and.returnValue(
      of([
        {
          id: '1AwZbA9p9SA4DuQkvWKa',
          data: {
            description: '',
            title: '',
            category: 'movie candies',
            employee: '',
            price: 0,
          },
        },
        {
          id: 'CwHjfmI5vecdmY7hjB35',
          data: {
            category: 'string',
            title: '',
            description: '',
            employee: '',
            price: 0,
          },
        },
      ])
    );
    service.getProductsList(true, 'id-store');
    expect(apiService.get).toHaveBeenCalledWith([
      'stores',
      'id-store',
      'products',
    ]);
  });

  it('quando è chiamato il prodotto mi aspetto un array ["stores", "id-store","products", "id-prodotto"]', () => {
    spyOn(apiService, 'get');
    service.getProductById('id-prodotto', 'id-store');
    expect(apiService.get).toHaveBeenCalledWith([
      'stores',
      'id-store',
      'products',
      'id-prodotto',
    ]);
  });

  it("mi aspetto di ricevere un elenco di categorie dato l'elenco dei prodotti", () => {
    const ArrProducts = [
      {
        id: '1AwZbA9p9SA4DuQkvWKa',
        data: {
          description: '',
          title: '',
          category: 'movie candies',
          employee: '',
          price: 0,
        },
      },
      {
        id: 'CwHjfmI5vecdmY7hjB35',
        data: {
          category: 'string',
          title: '',
          description: '',
          employee: '',
          price: 0,
        },
      },
    ];
    expect(service.getCategoryList(ArrProducts)).toEqual([
      'movie candies',
      'string',
    ]);
  });

  it('mi aspetto di ricevere un elenco di categorie senza ripetizioni', () => {
    const ArrProducts = [
      {
        id: '1AwZbA9p9SA4DuQkvWKa',
        data: {
          description: '',
          title: '',
          category: 'movie candies',
          employee: '',
          price: 0,
        },
      },
      {
        id: 'CwHjfmI5vecdmY7hjB35',
        data: {
          category: 'string',
          title: '',
          description: '',
          employee: '',
          price: 0,
        },
      },
      {
        id: 'CwHjfmI5vecdmY7hjB35',
        data: {
          category: 'string',
          title: '',
          description: '',
          employee: '',
          price: 0,
        },
      },
    ];
    expect(service.getCategoryList(ArrProducts)).toEqual([
      'movie candies',
      'string',
    ]);
  });
});
