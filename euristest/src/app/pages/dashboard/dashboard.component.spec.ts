import {
  async,
  ComponentFixture,
  TestBed,
  inject,
  tick,
} from '@angular/core/testing';

import { DashboardComponent } from './dashboard.component';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';
import { Routes } from 'src/app/enums/routes.enum';
import { StoreService } from '../store/service/store.service';
import { DashboardService } from './service/dashboard.service';
import { of } from 'rxjs';

describe('DashboardComponent', () => {
  let component: DashboardComponent;
  let fixture: ComponentFixture<DashboardComponent>;
  let router: Router;
  let storeService: StoreService;
  let service: DashboardService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DashboardComponent],
      providers: [StoreService, DashboardService],
      imports: [HttpClientModule, RouterTestingModule],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardComponent);
    component = fixture.componentInstance;
    router = TestBed.get(Router);
    service = TestBed.get(DashboardService);
    storeService = TestBed.get(StoreService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('se non è presente un current store faccio redirect agli store', () => {
    spyOn(component, 'isStoreSet').and.returnValue(false);
    spyOn(router, 'navigateByUrl');
    component.ngOnInit();
    expect(router.navigateByUrl).toHaveBeenCalledWith(Routes.STORE);
  });

  it('se è presente il currentStore allora chiamo la lista prodotti', () => {
    spyOn(service, 'getCurrentStore').and.returnValue('id-123');
    spyOn(service, 'getProductsList').and.returnValue(of([]));
    component.getProductsList();
    expect(service.getProductsList).toHaveBeenCalled();
  });
});
