import { Component, OnInit, Inject } from '@angular/core';
import { Product } from 'src/app/interfaces/api-response';
import {
  MatBottomSheetRef,
  MAT_BOTTOM_SHEET_DATA,
} from '@angular/material/bottom-sheet';

@Component({
  selector: 'app-product-dialog',
  templateUrl: './product-dialog.component.html',
  styleUrls: ['./product-dialog.component.scss'],
})
export class ProductDialogComponent implements OnInit {
  constructor(
    public dialogRef: MatBottomSheetRef<ProductDialogComponent>,
    @Inject(MAT_BOTTOM_SHEET_DATA) public data: Product
  ) {}

  ngOnInit(): void {}

  onSubmit(e) {
    console.log(e);
  }
}
