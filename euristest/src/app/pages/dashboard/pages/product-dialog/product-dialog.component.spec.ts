import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductDialogComponent } from './product-dialog.component';

import {
  MatBottomSheetRef,
  MAT_BOTTOM_SHEET_DATA,
  MatBottomSheetModule,
} from '@angular/material/bottom-sheet';
import { FormsModule } from '@angular/forms';

describe('ProductDialogComponent', () => {
  let component: ProductDialogComponent;
  let fixture: ComponentFixture<ProductDialogComponent>;

  const mockDialogRef = {
    data: {},
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProductDialogComponent],
      providers: [
        {
          provide: MatBottomSheetRef,
          useValue: mockDialogRef,
        },
        {
          provide: MAT_BOTTOM_SHEET_DATA,
          useValue: {},
        },
      ],
      imports: [MatBottomSheetModule, FormsModule],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
