import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductNewComponent } from './product-new.component';
import {
  MatBottomSheetModule,
  MatBottomSheetRef,
} from '@angular/material/bottom-sheet';
import { HttpClientModule } from '@angular/common/http';
import { DashboardService } from '../../service/dashboard.service';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { ProductNewDialogComponent } from '../product-new-dialog/product-new-dialog.component';

describe('ProductNewComponent', () => {
  let component: ProductNewComponent;
  let fixture: ComponentFixture<ProductNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProductNewComponent, ProductNewDialogComponent],
      providers: [
        DashboardService,
        {
          provide: MatBottomSheetRef,
          useValue: {},
        },
      ],
      imports: [
        MatBottomSheetModule,
        HttpClientModule,
        RouterTestingModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
        FormsModule,
        MatInputModule,
        MatFormFieldModule,
        MatAutocompleteModule,
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
