import { Component, OnInit } from '@angular/core';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { ProductNewDialogComponent } from '../product-new-dialog/product-new-dialog.component';
import { Router } from '@angular/router';
import { Routes } from 'src/app/enums/routes.enum';

@Component({
  selector: 'app-product-new',
  templateUrl: './product-new.component.html',
  styleUrls: ['./product-new.component.scss'],
})
export class ProductNewComponent implements OnInit {
  constructor(public bottomSheet: MatBottomSheet, private router: Router) {}

  ngOnInit(): void {
    const bottomSheet = this.bottomSheet.open(ProductNewDialogComponent);
    bottomSheet.afterDismissed().subscribe(() => {
      this.router.navigateByUrl(Routes.DASHBOARD);
    });
  }
}
