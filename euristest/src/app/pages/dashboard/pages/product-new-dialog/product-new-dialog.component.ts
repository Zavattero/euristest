import {
  Component,
  OnInit,
  OnDestroy,
  ViewChild,
  ElementRef,
} from '@angular/core';
import { MatBottomSheetRef } from '@angular/material/bottom-sheet';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DashboardService } from '../../service/dashboard.service';
import { Subscription } from 'rxjs';
import { Errors } from 'src/app/enums/errors.enum';

@Component({
  selector: 'app-product-new-dialog',
  templateUrl: './product-new-dialog.component.html',
  styleUrls: ['./product-new-dialog.component.scss'],
})
export class ProductNewDialogComponent implements OnInit, OnDestroy {
  newProductForm: FormGroup;
  categories: string[] = [];
  employees: string[] = [];

  private _getProductsList: Subscription;

  @ViewChild('autofocus') autofocusInput: ElementRef;

  constructor(
    private formBuilder: FormBuilder,
    private dashboardService: DashboardService,
    public dialogRef: MatBottomSheetRef<ProductNewDialogComponent>
  ) {}

  ngOnInit(): void {
    this.getData();

    this.newProductForm = this.createForm();
  }

  createForm() {
    return this.formBuilder.group({
      title: ['', Validators.required],
      category: ['', Validators.required],
      price: [
        '',
        [Validators.required, Validators.pattern('[0-9]*(.[0-9]{2,})?')],
      ],
      employee: ['', Validators.required],
      description: ['', Validators.required],
    });
  }

  getData() {
    this._getProductsList = this.dashboardService
      .getProductsList(false)
      .subscribe((res) => {
        this.categories = this.dashboardService.getCategoryList(res);
        this.employees = this.dashboardService.getEmployeesList(res);
      });
  }

  getError(el) {
    const errorsArr = Object.keys(this.newProductForm.get(el).errors);
    const errorsList = errorsArr.map((el) => {
      return Errors[el];
    });

    return errorsList.join(', ');
  }

  onSubmit(e?) {
    const values = this.newProductForm.value;
    console.log('submiiit', values);
    this.dashboardService.saveNewProduct(values).subscribe(
      (res: string) => {
        this.dashboardService.setNewProductId(res);
        this.dialogRef.dismiss();
      },
      (err) => {
        console.error(err);
      }
    );
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this._getProductsList.unsubscribe();
  }
}
