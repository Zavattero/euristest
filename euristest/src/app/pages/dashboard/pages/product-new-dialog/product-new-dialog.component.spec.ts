import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductNewDialogComponent } from './product-new-dialog.component';
import {
  MatBottomSheetRef,
  MAT_BOTTOM_SHEET_DATA,
  MatBottomSheetModule,
} from '@angular/material/bottom-sheet';
import { ReactiveFormsModule, FormsModule, FormBuilder } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatTextareaAutosize, MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';

describe('ProductNewDialogComponent', () => {
  let component: ProductNewDialogComponent;
  let fixture: ComponentFixture<ProductNewDialogComponent>;

  const mockDialogRef = {};

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProductNewDialogComponent],
      providers: [
        {
          provide: MatBottomSheetRef,
          useValue: mockDialogRef,
        },
        {
          provide: MAT_BOTTOM_SHEET_DATA,
          useValue: {},
        },
        FormBuilder,
      ],
      imports: [
        MatBottomSheetModule,
        ReactiveFormsModule,
        RouterTestingModule,
        HttpClientModule,
        BrowserAnimationsModule,
        FormsModule,
        MatAutocompleteModule,
        MatInputModule,
        MatFormFieldModule,
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductNewDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
