import {
  async,
  ComponentFixture,
  TestBed,
  tick,
  fakeAsync,
} from '@angular/core/testing';

import { ProductComponent } from './product.component';
import { RouterTestingModule } from '@angular/router/testing';
import { DashboardService } from '../../service/dashboard.service';
import { Router } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { of, throwError } from 'rxjs';
import {
  MatBottomSheetModule,
  MatBottomSheetRef,
  MAT_BOTTOM_SHEET_DATA,
} from '@angular/material/bottom-sheet';

describe('ProductComponent', () => {
  let component: ProductComponent;
  let fixture: ComponentFixture<ProductComponent>;
  let service: DashboardService;
  let router: Router;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProductComponent],
      providers: [
        DashboardService,
        {
          provide: MatBottomSheetRef,
          useValue: {},
        },
      ],
      imports: [RouterTestingModule, HttpClientModule, MatBottomSheetModule],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductComponent);
    component = fixture.componentInstance;
    service = TestBed.get(DashboardService);
    router = TestBed.get(Router);

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  // it('se il prodotto esiste mi apre il modale', () => {
  //   spyOn(component, 'showDialog');
  //   spyOn(service, 'getProductById').and.returnValue(of({}));
  //   component.getProduct('id-123');
  //   expect(component.showDialog).toHaveBeenCalled();
  // });

  // it('se il prodotto non esiste mi apre il modale di errore', () => {
  //   spyOn(component, 'showDialogError');
  //   spyOn(service, 'getProductById').and.returnValue(
  //     throwError({ status: 404 })
  //   );
  //   component.getProduct('id-non-esistente');
  //   expect(component.showDialogError).toHaveBeenCalled();
  // });
});
