import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DashboardService } from '../../service/dashboard.service';
import { Product } from 'src/app/interfaces/api-response';
import { ProductDialogComponent } from '../product-dialog/product-dialog.component';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { Routes } from 'src/app/enums/routes.enum';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss'],
})
export class ProductComponent implements OnInit {
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private dashboardService: DashboardService,
    public bottomSheet: MatBottomSheet
  ) {}

  ngOnInit(): void {
    this.route.paramMap.subscribe((res) => {
      const id = res.get('id');
      this.getProduct(id);
      this.dashboardService.setCurrentProductId(id);
    });
  }

  getProduct(id) {
    this.dashboardService.getProductById(id).subscribe(
      (res: Product) => {
        this.showDialog(res);
      },
      (err) => {
        this.showDialogError(err);
      }
    );
  }

  showDialog(productData: Product) {
    const dialog = this.bottomSheet.open(ProductDialogComponent, {
      data: productData,
    });

    dialog.afterDismissed().subscribe(() => {
      this.dashboardService.setCurrentProductId('');
      this.router.navigateByUrl(Routes.DASHBOARD);
    });
  }

  showDialogError(error) {
    console.error(error);
  }
}
