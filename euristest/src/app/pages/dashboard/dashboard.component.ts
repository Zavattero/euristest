import { Component, OnInit, OnDestroy } from '@angular/core';
import { DashboardService } from './service/dashboard.service';
import { Router } from '@angular/router';
import { Routes } from 'src/app/enums/routes.enum';
import { Products, Store, StoreLayout } from 'src/app/interfaces/api-response';
import { Icons } from 'src/app/enums/icons.enum';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material/icon';
import { MenuService } from 'src/app/services/menu.service';
import { Subscription } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { ProductDeleteDialogComponent } from 'src/app/components/dashboard/product-delete-dialog/product-delete-dialog.component';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit, OnDestroy {
  private currentStore: string;
  loading: boolean = false;
  productList: Products[] = [];
  storeData: StoreLayout;
  viewMode: string;
  currentProductId: string;
  newUrl: string;
  newProductId: string;
  isMultiSelectActive: boolean = false;
  isSelectAll: boolean = true;
  isEmpty: boolean = true;
  private selectedIds: Set<string>;
  private _getViewMode: Subscription;
  private _getCurrentProductId: Subscription;
  private _getProductsList: Subscription;
  private _getStoreData: Subscription;
  private _getNewProductId: Subscription;

  // @ViewChild('visualizzazione') visualizzazioneRef: ElementRef;
  constructor(
    private dashboardService: DashboardService,
    private router: Router,
    public dialog: MatDialog,
    private menuService: MenuService,
    iconRegistry: MatIconRegistry,
    sanitizer: DomSanitizer
  ) {
    iconRegistry.addSvgIcon(
      'belly',
      sanitizer.bypassSecurityTrustResourceUrl('assets/svg/belly.svg')
    );
  }

  ngOnInit(): void {
    this.menuService.setTitle('Dashboard');
    if (this.isStoreSet()) {
      this._getViewMode = this.dashboardService
        .getViewMode()
        .subscribe((res) => (this.viewMode = res));

      this._getCurrentProductId = this.dashboardService
        .getCurrentProductId()
        .subscribe((res) => (this.currentProductId = res));

      this.getProductsList();
      this.getStoreData();
      this.newUrl = this.setNewUrl();
      this.getNewProductId();
      this.getMultiSelect();
    } else {
      this.router.navigateByUrl(Routes.STORE);
    }
  }

  isStoreSet() {
    this.currentStore = this.dashboardService.getCurrentStore();
    return !(this.currentStore === null || this.currentStore === '');
  }

  getProductsList() {
    this.loading = true;
    this._getProductsList = this.dashboardService.getProductsList().subscribe(
      (res: Products[]) => {
        this.loading = false;
        this.productList = res;
        console.log(this.productList);
      },
      (err) => {
        this.loading = false;
        this.showError(err);
      }
    );
  }

  getStoreData() {
    this._getStoreData = this.dashboardService
      .getStoreData()
      .subscribe((res: Store) => {
        this.storeData = res;
        this.storeData.icon = Icons[this.storeData.category];
      });
  }

  showError(error) {
    console.error(error);
  }

  setNewUrl() {
    return [Routes.DASHBOARD, Routes.NEW].join('/');
  }

  getNewProductId() {
    this._getNewProductId = this.dashboardService
      .getNewProductId()
      .subscribe((res) => {
        this.newProductId = res;
        this.dashboardService.callProductList();
      });
  }

  getMultiSelect() {
    this.dashboardService
      .getMultipleSelect()
      .subscribe((res) => (this.isMultiSelectActive = res));

    this.dashboardService.getMultipleSelectArr().subscribe((res) => {
      this.isSelectAll = res.size !== this.productList.length;
      this.isEmpty = res.size === 0;
      this.selectedIds = res;
    });
  }

  deleteMultiple(e) {
    const total = this.selectedIds.size;
    let counter = 0;
    const filtered = this.productList
      .filter((el) => {
        return this.selectedIds.has(el.id);
      })
      .map((el) => el.data);
    console.log(filtered);
    const dialogRef = this.dialog.open(ProductDeleteDialogComponent, {
      data: {
        productData: filtered,
      },
    });

    dialogRef.afterClosed().subscribe((hasToRemove) => {
      if (hasToRemove) {
        this.selectedIds.forEach((el) => {
          this.dashboardService.deleteProduct(el).subscribe((res) => {
            console.log('deleted', el);
            counter++;
            if (counter === total) {
              this.dashboardService.setMultipleSelect(false);
              this.dashboardService.callProductList();
              console.log('eliminati tutti');
            }
          });
        });
      }
    });
  }
  selectAll(e) {
    const arrId = this.productList.map((el) => {
      return el.id;
    });
    this.dashboardService.toggleAllMultiple(arrId);
  }
  disableMultiple(e?) {
    this.dashboardService.setMultipleSelect(false);
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this._getViewMode.unsubscribe();
    this._getCurrentProductId.unsubscribe();
    this._getProductsList.unsubscribe();
    this._getStoreData.unsubscribe();
    this._getNewProductId.unsubscribe();
  }
}
