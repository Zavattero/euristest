import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from './dashboard.component';
import { ProductComponent } from './pages/product/product.component';
import { Routes as R } from 'src/app/enums/routes.enum';
import { ProductNewComponent } from './pages/product-new/product-new.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    children: [
      { path: R.NEW, component: ProductNewComponent },
      { path: ':id/' + R.REVIEW, component: ProductComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashboardRoutingModule {}
