import {
  async,
  ComponentFixture,
  TestBed,
  tick,
  fakeAsync,
} from '@angular/core/testing';

import { StoreComponent } from './store.component';
import { StoreService } from './service/store.service';
import { HttpClientModule } from '@angular/common/http';
import { ApiService } from 'src/app/services/api.service';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
import { Routes } from 'src/app/enums/routes.enum';

describe('StoreComponent', () => {
  let component: StoreComponent;
  let fixture: ComponentFixture<StoreComponent>;
  let service: StoreService;
  let router: Router;

  const storeListMock = [
    {
      id: 'ijpxNJLM732vm8AeajMR',
      data: {
        name: 'Dolci di Piera',
        employees: ['Aldo', 'Giovanni', 'Giacomo'],
        category: 'pasticceria',
      },
    },
  ];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [StoreComponent],
      providers: [StoreService, ApiService],
      imports: [HttpClientModule, RouterTestingModule],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StoreComponent);
    component = fixture.componentInstance;
    service = TestBed.inject(StoreService);
    router = TestBed.get(Router);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('al primo atterraggio mi aspetto che il current store sia nullo', () => {
    service.deleteCurrentStore();
    component.getStore();
    expect(component.currentStore).toBeNull();
  });

  it("se c'è un solo store ed è quello di default mi aspetto un redirect", fakeAsync(() => {
    service.deleteCurrentStore();
    environment.store.defaultid = 'ijpxNJLM732vm8AeajMR';
    spyOn(service, 'getStoreList').and.returnValue(of(storeListMock));
    spyOn(router, 'navigateByUrl');
    component.ngOnInit();
    tick();
    expect(router.navigateByUrl).toHaveBeenCalledWith(Routes.DASHBOARD);
  }));
});
