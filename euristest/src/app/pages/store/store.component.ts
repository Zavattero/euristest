import { Component, OnInit } from '@angular/core';
import { StoreService } from './service/store.service';
import { Stores } from 'src/app/interfaces/api-response';
import { environment } from 'src/environments/environment';
import { ThrowStmt } from '@angular/compiler';
import { Route, Router, ROUTES } from '@angular/router';
import { Routes } from 'src/app/enums/routes.enum';
import { MenuService } from 'src/app/services/menu.service';

@Component({
  selector: 'app-store',
  templateUrl: './store.component.html',
  styleUrls: ['./store.component.scss'],
})
export class StoreComponent implements OnInit {
  currentStore: string | null;
  constructor(
    private storeService: StoreService,
    private router: Router,
    private menuService: MenuService
  ) {}

  ngOnInit(): void {
    this.menuService.setTitle('Store');
    this.getStore();
  }

  getStore() {
    this.currentStore = this.storeService.getcurrentStore();
    if (this.currentStore === null) {
      this.storeService.getStoreList().subscribe((res: Stores[]) => {
        if (res.length === 1 && res[0].id === environment.store.defaultid) {
          this.storeService.setCurrentStore(res[0].id);
          this.storeService.setCurrentStoreData(res[0].data);
          this.goToDashboard();
        } else {
          console.log('aaaaaaaaaabbbbba');
        }
      });
    } else {
      this.storeService.getStoreById(this.currentStore).subscribe((res) => {
        this.storeService.setCurrentStoreData(res);
        this.goToDashboard();
      });
    }
  }

  goToDashboard() {
    this.router.navigateByUrl(Routes.DASHBOARD);
  }
}
