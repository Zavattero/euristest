import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { EndpointsService } from 'src/app/services/endpoints.service';
import { ApiService } from 'src/app/services/api.service';
import { Stores, Store } from 'src/app/interfaces/api-response';

@Injectable({
  providedIn: 'root',
})
export class StoreService {
  private currentStoreId: string | null = null;
  private currentStore: Store;

  constructor(
    private endpointService: EndpointsService,
    private apiService: ApiService
  ) {}

  getcurrentStore(): string | null {
    this.currentStoreId = localStorage.getItem('currentStoreId') || null;
    return this.currentStoreId;
  }
  setCurrentStore(id: string): void {
    localStorage.setItem('currentStoreId', id);
    this.currentStoreId = id;
  }

  deleteCurrentStore(): string | null {
    localStorage.removeItem('currentStoreId');
    return this.getcurrentStore();
  }

  getStoreList(): Observable<Stores[]> {
    const storeUrl = this.endpointService.store();
    return this.apiService.get(storeUrl);
  }

  getStoreById(id): Observable<Store> {
    const storeUrl = this.endpointService.store(id);
    return this.apiService.get(storeUrl);
  }

  setCurrentStoreData(storeData: Store) {
    this.currentStore = storeData;
  }

  getCurrentStoreData() {
    return this.currentStore;
  }
}
