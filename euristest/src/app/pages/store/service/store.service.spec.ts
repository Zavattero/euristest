import { TestBed } from '@angular/core/testing';

import { StoreService } from './store.service';
import { EndpointsService } from 'src/app/services/endpoints.service';
import { ApiService } from 'src/app/services/api.service';
import { HttpClientModule } from '@angular/common/http';

describe('StoreService', () => {
  let service: StoreService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EndpointsService, ApiService],
      imports: [HttpClientModule],
    });
    service = TestBed.inject(StoreService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
