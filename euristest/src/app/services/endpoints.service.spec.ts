import { TestBed } from '@angular/core/testing';

import { EndpointsService } from './endpoints.service';
import { Endpoints } from '../enums/endpoints.enum';
describe('EndpointsService', () => {
  let service: EndpointsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EndpointsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('mi aspetto che il path degli store sia [stores]', () => {
    expect(service.store()).toEqual(['stores']);
  });

  it('mi aspetto che il path degli store sia [stores, id-123]', () => {
    expect(service.store('id-123')).toEqual(['stores', 'id-123']);
  });

  it('mi aspetto che il path degli store sia [test, stores, id-123]', () => {
    expect(service.store('id-123', ['test'])).toEqual([
      'test',
      'stores',
      'id-123',
    ]);
  });

  it('mi aspetto che il path del product sia [stores, id-123, products]', () => {
    expect(service.product('id-123')).toEqual(['stores', 'id-123', 'products']);
  });

  it('mi aspetto che il path del product sia [stores, id-123, products, p1]', () => {
    expect(service.product('id-123', 'p1')).toEqual([
      'stores',
      'id-123',
      'products',
      'p1',
    ]);
  });

  it('mi aspetto che il path delle categories sia [stores, id-123, stats, categories]', () => {
    expect(service.categoryStats('id-123')).toEqual([
      'stores',
      'id-123',
      'stats',
      'categories',
    ]);
  });
});
