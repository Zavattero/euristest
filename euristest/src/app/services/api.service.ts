import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  constructor(private http: HttpClient) {}

  getApiUrl(path: string[], baseUrl = environment.apiUrl) {
    return [baseUrl, ...path].join('/');
  }

  get(path: string[], options = {}): Observable<any> {
    const url = this.getApiUrl(path);
    return this.http.get(url, options);
  }

  post(path: string[], body: any, options = {}) {
    const url = this.getApiUrl(path);
    return this.http.post(url, body, options);
  }

  delete(path: string[], options = {}) {
    const url = this.getApiUrl(path);
    return this.http.delete(url, options);
  }
}
