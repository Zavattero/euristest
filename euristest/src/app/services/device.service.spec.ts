import { TestBed } from '@angular/core/testing';

import { DeviceService } from './device.service';
import { BreakpointObserver } from '@angular/cdk/layout';

describe('DeviceService', () => {
  let service: DeviceService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BreakpointObserver],
    });
    service = TestBed.inject(DeviceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
