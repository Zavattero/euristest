import { Injectable } from '@angular/core';
import { Endpoints } from '../enums/endpoints.enum';

@Injectable({
  providedIn: 'root',
})
export class EndpointsService {
  constructor() {}

  store(id?: string, currentPath = []) {
    const storePath: Array<string> = [Endpoints.STORES];
    if (id) {
      storePath.push(id);
    }
    return [...currentPath, ...storePath];
  }

  product(storeId: string, productId?: string, currentPath = []) {
    const productPath = [...this.store(storeId), Endpoints.PRODUCTS];
    if (productId) {
      productPath.push(productId);
    }
    return [...currentPath, ...productPath];
  }

  private stats(storeId: string, currentPath = []) {
    const statsPath = [...this.store(storeId), Endpoints.STATS];
    return [...currentPath, ...statsPath];
  }

  categoryStats(storeId: string, currentPath = []) {
    return [...currentPath, ...this.stats(storeId), Endpoints.CATEGORIES];
  }
}
