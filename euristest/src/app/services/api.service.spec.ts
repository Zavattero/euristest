import { TestBed } from '@angular/core/testing';

import { ApiService } from './api.service';
import { HttpClientModule } from '@angular/common/http';

describe('ApiService', () => {
  let service: ApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
    });
    service = TestBed.inject(ApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it("mi aspetto di ottenere l'url delle API", () => {
    expect(service.getApiUrl([], 'https://google.com')).toBe(
      'https://google.com'
    );
  });

  it("mi aspetto l'che path e url vengano mergiati assieme", () => {
    expect(service.getApiUrl(['test'], 'https://google.com')).toBe(
      'https://google.com/test'
    );
  });
});
