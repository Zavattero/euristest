import { TestBed } from '@angular/core/testing';

import { MenuService } from './menu.service';
import { DeviceService } from './device.service';

describe('MenuService', () => {
  let service: MenuService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DeviceService],
    });
    service = TestBed.inject(MenuService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
