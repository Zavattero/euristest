import { Injectable } from '@angular/core';
import {
  BreakpointObserver,
  Breakpoints,
  BreakpointState,
} from '@angular/cdk/layout';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class DeviceService {
  private breakPoints: Array<string> = [
    Breakpoints.Handset,
    Breakpoints.Tablet,
    Breakpoints.Web,
    '(orientation: portrait)',
    '(orientation: landscape)',
  ];

  constructor(private breakpointObserver: BreakpointObserver) {}

  checkBreakpoints(): Observable<BreakpointState> {
    return this.breakpointObserver.observe(this.breakPoints);
  }

  isMobile(): boolean {
    return this.breakpointObserver.isMatched(Breakpoints.Handset);
  }
  isTablet(): boolean {
    return this.breakpointObserver.isMatched(Breakpoints.Tablet);
  }
  isDesktop(): boolean {
    return this.breakpointObserver.isMatched(Breakpoints.Web);
  }
}
