import { Injectable } from '@angular/core';
import { DeviceService } from './device.service';
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class MenuService {
  private _isOpen: BehaviorSubject<boolean> = new BehaviorSubject(false);
  private _currentlyOpen: boolean;
  private _hasBackdrop: BehaviorSubject<boolean> = new BehaviorSubject(false);
  private _pageTitle: BehaviorSubject<string> = new BehaviorSubject(
    'EurisTest'
  );

  constructor(private deviceService: DeviceService) {}

  checkIsOpen() {
    return this.deviceService.checkBreakpoints().subscribe((_res) => {
      this._currentlyOpen = this.deviceService.isDesktop();
      this._hasBackdrop.next(!this.deviceService.isDesktop());
      this._isOpen.next(this._currentlyOpen);
    });
  }

  isOpen(): Observable<boolean> {
    return this._isOpen;
  }

  hasBackdrop(): Observable<boolean> {
    return this._hasBackdrop;
  }

  toggleOpen() {
    this._currentlyOpen = !this._currentlyOpen;
    this._isOpen.next(this._currentlyOpen);
  }

  close() {
    this._currentlyOpen = false;
    this._isOpen.next(this._currentlyOpen);
  }

  getTitle(): Observable<string> {
    return this._pageTitle.asObservable();
  }

  setTitle(title: string): void {
    this._pageTitle.next(title);
  }
}
