import { Component, OnInit } from '@angular/core';
import { MenuService } from './services/menu.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'euristest';

  isMenuOpen: boolean = false;
  hasBackdrop: boolean = false;

  constructor(private menuService: MenuService) {}

  ngOnInit(): void {
    this.checkMenu();
  }

  checkMenu() {
    // this.menuService.checkIsOpen();
    // this.menuService.isOpen().subscribe((res) => {
    //   this.isMenuOpen = res;
    // });
    // this.menuService.hasBackdrop().subscribe((res) => (this.hasBackdrop = res));
  }

  backdropClose(e) {
    this.menuService.close();
  }
}
