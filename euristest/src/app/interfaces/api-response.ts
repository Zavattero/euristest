export interface Stores {
  id: string;
  data: Store;
}

export interface Store {
  name: string;
  employees: string[];
  category: string;
}

export interface StoreLayout extends Store {
  icon?: 'string';
}

export interface Products {
  id: string;
  data: Product;
}
export interface Product {
  title: string;
  category: string;
  price: number;
  employee: string;
  description?: string;
  reviews?: string[];
}

export interface ProductSave {
  title: string;
  category: string;
  price: number;
  employee: string;
  description: string;
}
