export enum Errors {
  required = 'Il campo è obbligatorio',
  pattern = 'Il valore deve essere un numero intero o con due decimali (1 o 1.12)',
}
