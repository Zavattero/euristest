export enum Endpoints {
  STORES = 'stores',
  PRODUCTS = 'products',
  STATS = 'stats',
  CATEGORIES = 'categories',
}
